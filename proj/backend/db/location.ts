import mongoose from 'mongoose';
import { UserLocation } from 'lib/geo';

export type UserLocationDocumentType = mongoose.Document & UserLocation;

const userLocationSchema = new mongoose.Schema<UserLocation>({
    user: { type: Number, required: true },
    lng: { type: Number, required: true },
    lat: { type: Number, required: true }
});

export const UserLocationDocument = mongoose.model<UserLocationDocumentType>('UserLocation', userLocationSchema);
