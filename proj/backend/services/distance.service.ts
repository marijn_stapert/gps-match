import { UserLocation } from 'lib/geo';

export function distanceBetween(coord1: UserLocation, coord2: UserLocation): number {
    const R = 6371e3; // metres
    const pi = Math.PI;
    const φ1 = coord1.lat * (pi / 180);
    const φ2 = coord2.lat * (pi / 180);
    const Δφ = (coord2.lat - coord1.lat) * (pi / 180);
    const Δλ = (coord2.lng - coord1.lng) * (pi / 180);

    const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return R * c;
}
