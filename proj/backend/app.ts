// Express specific imports
import * as dotenv from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import session from 'express-session';
import morgan from 'morgan';
import passport from 'passport';

// Mongo
import mongoose from 'mongoose';

// Controllers
import * as api from './controllers/api';
import * as user from './controllers/user';

// Configurations
import * as passportConfig from './config/passport';

// Mongo config
const connStr = 'mongodb+srv://test_admin:blaat@test-o46qk.azure.mongodb.net/test?retryWrites=true&w=majority';
mongoose
    .connect(connStr, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('   Mongo db conn established');
    })
    .catch(err => {
        console.error(' MongoDB connection error. Please make sure MongoDB is running. ' + err);
        process.exit();
    });

// Express configuration
dotenv.config();
const app = express();
app.set('env', process.env.ENV || 'development');
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// Middleware
app.use(cors());
app.use(morgan(process.env.LOGGING || 'dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
    session({
        secret: process.env.SESSION_SECRET || 'keyboard cat',
        cookie: {},
        resave: false,
        saveUninitialized: false
    })
);
app.use(passport.initialize());
app.use(passport.session());

// Routing
// app.use('/api', passportConfig.isAuthenticated, api.router);
app.use('/api', api.router);
app.use('/user', user.router);

export default app;
