import { Request, Response, NextFunction } from 'express';

import passport from 'passport';
import passportLocal from 'passport-local';

const LocalStrategy = passportLocal.Strategy;

// Db config -> turn into mongodb setup
const db = require('../db');

passport.serializeUser<any, any>((user, done) => {
    done(undefined, user.id);
});

passport.deserializeUser((id, done) => {
    db.users.findById(id, (err, user) => {
        done(err, user);
    });
});

passport.use(
    new LocalStrategy((username, password, done) => {
        db.users.findByUsername(username, (err, user) => {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false);
            }
            if (user.password !== password) {
                return done(null, false);
            }
            return done(null, user);
        });
    })
);

/**
 * Login Required middleware.
 */
export const isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
    if (req.isAuthenticated()) {
        return next();
    }
    res.statusCode = 401;
    next();
};
