import { Request, Response, Router, NextFunction } from 'express';
import { UserLocation } from 'lib/geo';

import { distanceBetween } from '../services/distance.service';

import { UserLocationDocument, UserLocationDocumentType } from '../db/location';

export const router = Router();

// middleware that is specific to this router
router.use((req: Request, res: Response, next: NextFunction) => {
    next();
});

router.get('/', (req: Request, res: Response) => {
    res.json({ ok: true });
});

router.get('/location/:userId', async (req: Request, res: Response) => {
    const userLocation: UserLocationDocumentType = await UserLocationDocument.findOne({ user: parseInt(req.params.userId, 10) });
    const otherUserLocations: UserLocationDocumentType[] = await UserLocationDocument.find();

    if (userLocation) {
        const nearbyLocations = otherUserLocations
            .filter(otherUser => otherUser.user !== userLocation.user)
            .filter(otherUser => distanceBetween(userLocation, otherUser) < 50000);
        res.json(nearbyLocations);
    }
});

router.post('/location', (req: Request, res: Response) => {
    const newLocation = new UserLocationDocument({
        lat: req.body.lat,
        lng: req.body.lng,
        user: parseInt(req.body.user, 10)
    });
    newLocation.save();
    res.json({ ok: true });
});
