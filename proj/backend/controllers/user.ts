import { Request, Response, Router, NextFunction } from 'express';
import passport from 'passport';
import * as passportConfig from '../config/passport';

export const router = Router();

// Define routes.
router.get('/', (req: Request, res: Response) => {
        res.render('home', { user: req.user });
});

router.get('/login', (req: Request, res: Response) => {
        res.render('login');
});

router.post('/login',
  passport.authenticate('local', { failureRedirect: '/user' }),
  (req: Request, res: Response) => {
    res.redirect('/user');
});

router.get('/logout',
  (req: Request, res: Response) => {
    req.logout();
    res.redirect('/user');
  });

router.get('/profile',
  passportConfig.isAuthenticated,
  (req: Request, res: Response) => {
    res.render('profile', { user: req.user });
});
