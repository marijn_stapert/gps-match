import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { GoogleMapsModule } from '@angular/google-maps';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { LandingComponent } from './landing/landing.component';
import { MapComponent } from './components/map/map.component';
import { LocationComponent } from './location/location.component';
import { ToMarkerPipe } from './pipes/to-marker.pipe';

@NgModule({
    declarations: [AppComponent, LandingComponent, LayoutComponent, MapComponent, LocationComponent, ToMarkerPipe],
    imports: [BrowserModule, GoogleMapsModule, AppRoutingModule, HttpClientModule],
    bootstrap: [AppComponent]
})
export class AppModule {}
