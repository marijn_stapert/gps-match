import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styles: [
        `
            .container {
                display: flex;
                flex-direction: row;
            }

            .menu {
                flex-grow: 1;
            }

            .content {
                flex-grow: 3;
            }
        `
    ]
})
export class LayoutComponent {}
