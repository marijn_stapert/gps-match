import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { UserLocation } from 'lib/geo';
import { LocationService } from 'frontend/app/services/location.service';
import { Observable } from 'rxjs';
import { MapMarker, GoogleMap, MapInfoWindow } from '@angular/google-maps';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styles: [``]
})
export class MapComponent implements OnInit {
    @ViewChild(GoogleMap, { static: false }) map: GoogleMap;
    @ViewChild(MapInfoWindow, { static: false }) info: MapInfoWindow;

    @Input()
    userLocations = [];

    @Output()
    clickLocation: EventEmitter<google.maps.LatLng> = new EventEmitter<google.maps.LatLng>();

    center: google.maps.LatLng;

    zoom = 10;
    infoContent = '';

    constructor(private locationService: LocationService) {}

    ngOnInit(): void {
        this.locationService.clientLocation.asObservable().subscribe(location => {
            const { lat, lng } = location;
            this.center = new google.maps.LatLng(lat, lng);
        });
    }

    click(event: google.maps.MouseEvent): void {
        this.clickLocation.emit(new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()));
    }

    openInfo(marker: MapMarker, content: any): void {
        this.infoContent = content;
        this.info.open(marker);
    }
}
