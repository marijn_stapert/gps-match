import { Pipe, PipeTransform } from '@angular/core';
import { UserLocation } from 'lib/geo';
import { LocationService } from '../services/location.service';

@Pipe({ name: 'toMarker' })
export class ToMarkerPipe implements PipeTransform {
    transform(values: UserLocation[]): any[] {
        if (values) {
            return values.map(val => {
                return LocationService.newMarker(val);
            });
        }
        return [];
    }
}
