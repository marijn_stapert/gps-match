import { Component, OnInit } from '@angular/core';
import { LocationService } from 'frontend/app/services/location.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserLocation } from 'lib/geo';
import { UserService } from '../services/user.service';

@Component({
    selector: 'app-location',
    templateUrl: './location.component.html',
    styles: [``]
})
export class LocationComponent implements OnInit {
    userLocations: Observable<UserLocation[]> = this.locationService.userLocations;
    user = this.userService.user;

    constructor(private locationService: LocationService, private userService: UserService) {}

    ngOnInit(): void {
        this.locationService.updateClientLocation();
        this.locationService.getUserLocations();
    }

    updateUser(user: string): void {
        this.userService.updateUser(parseInt(user, 10));
        this.locationService.getUserLocations();
    }

    postLocation(location: google.maps.LatLng): void {
        this.locationService.postLocation({ lat: location.lat(), lng: location.lng(), user: this.userService.user });
    }
}
