import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    user = 0;

    updateUser(user: number): void {
        this.user = user;
    }
}
