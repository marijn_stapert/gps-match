import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { UserLocation } from 'lib/geo';
import { environment } from 'frontend/environments/environment';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class LocationService {
    clientLocation: BehaviorSubject<UserLocation> = new BehaviorSubject<UserLocation>({
        user: 0,
        lat: 0,
        lng: 0
    });

    userLocations: BehaviorSubject<UserLocation[]> = new BehaviorSubject<UserLocation[]>([]);

    constructor(private userService: UserService, private client: HttpClient) {}

    static newMarker(location: UserLocation): any {
        return {
            position: {
                lat: location.lat,
                lng: location.lng
            },
            label: {
                color: 'red',
                text: 'Marker label ' + location.user
            },
            title: 'Marker title ' + location.user,
            info: 'Marker info ' + location.user,
            options: {
                animation: google.maps.Animation.BOUNCE
            }
        };
    }

    postLocation(location: UserLocation): void {
        this.client.post<UserLocation>(`${environment.endpoint}/location`, location).subscribe(() => {
            this.getUserLocations();
        });
    }

    getUserLocations(): void {
        this.client.get<any>(`${environment.endpoint}/location/${this.userService.user}`).subscribe(locations => {
            this.userLocations.next(Array.from(locations));
        });
    }

    updateClientLocation(): void {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position: Position) => {
                    this.clientLocation.next({
                        user: this.userService.user,
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    });
                },
                this.errorHandler,
                { timeout: 60000 }
            );
        } else {
            alert('Sorry, browser does not support geolocation!');
        }
    }

    private errorHandler(err: PositionError): void {
        if (err.code === 1) {
            alert('Error: Access is denied!');
        } else if (err.code === 2) {
            alert('Error: Position is unavailable!');
        }
    }
}
