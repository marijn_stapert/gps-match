export interface UserLocation {
    user: number;
    lng: number;
    lat: number;
}
